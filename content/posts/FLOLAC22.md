+++
author = "Lambdaww"
title = "FLOLAC 2022"
date = "2022-08-01"
description = ""
tags = [
    "sponsorship",
]
+++

We are delighted to announce that we are one of the sponsors for FLOLAC 2022, contributing support to the functional world and the formal methods community.

![](../flolac22.png)

Please see more on their website: https://flolac.iis.sinica.edu.tw/
