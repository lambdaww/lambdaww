+++
author = "Lambdaww"
title = "FLOLAC 2024"
date = "2024-09-01"
description = ""
tags = [
    "sponsorship",
]
+++

We are delighted to announce that we are one of the sponsors for FLOLAC 2024, contributing support to the functional world and the formal methods community.

![](../flolac24.jpg)

Please see more on their website: https://flolac.iis.sinica.edu.tw/
