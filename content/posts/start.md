+++
author = "Lambdaww"
title = "緣起"
date = "2020-11-23"
description = ""
tags = [
    "startup",
]
+++

Founded in November 2020, Lambdaww is a software and hardware solutions company with a focus on functional programming languages and formal methods. Our current main business areas include:

+ Software development with good quality and high security
+ Smart contract architecture and design
+ Planning and landing of IoT total solutions

--

入山山成立於 2020 年 11 月，是聚焦於函數式程式語言以及形式方法的軟硬體解決方案公司。目前主要業務為

+ 提供高安全性之軟體系統的開發與協作
+ 智慧合約之架構與設計
+ 規劃與落實 IoT 解決方案
